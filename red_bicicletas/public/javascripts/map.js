//L.map('El nombre de la etiqueta donde se va a mostrar').setView([latitud, longitud], nivel de zoom(numero XD))
var map = L.map('main_map').setView([6.217, -75.567], 12);

//Temas de copyright y agrega el mapa con addTo
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> Contributors'
}).addTo(map);

//Marcas en el mapa con marker([latitud, longitud]).addTo(mapa)
$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result) {
        console.log(result);
        result.bicicletas.forEach(function(bici) {
            L.marker(bici.ubicacion, { title: bici.id }).addTo(map);

        });
    }
});