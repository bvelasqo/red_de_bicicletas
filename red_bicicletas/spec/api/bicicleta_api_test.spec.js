const request = require('request');
var Bicicleta = require('../../Models/bicicletas');
var server = require('../../bin/www');


describe('Api', () => {
    describe('Get Bicicletas /', () => {
        it('status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, 'rojo', 'urbana', [6.2260568, -75.5999279]);
            Bicicleta.add(a);
            request.get('http://localhost:3000/api/bicicletas', function(error, response, body) {
                expect(response.statusCode).toBe(200);
            })
        });
    });
    describe('Post Bicicletas /Create', () => {
        it('Status 200', (done) => {
            var headers = { 'Content-Type': 'application/json' };
            var aBici = '{ "id": 10, "color": "rojo", "modelo": "urbana", "latitud": -34, "longitud": -54 }';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                expect(Bicicleta.findById(10).color).toBe("rojo")
                done();
            });
        });

    });
});