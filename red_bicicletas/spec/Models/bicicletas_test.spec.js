var Bicicleta = require('../../Models/bicicletas');

beforeEach(() => {
    Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis', () => {
    it('Comienza vacia', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', () => {
    it('Agregamos una', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana', [6.2260568, -75.5999279]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);

    });
});

describe('Bicicleta.findById', () => {
    it('Encontrar bicicleta con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana', [6.2260568, -75.5999279]);
        var b = new Bicicleta(2, 'azul', 'urbana', [6.23083494, -75.55701256]);
        Bicicleta.add(a);
        Bicicleta.add(b);
        var targetBici = Bicicleta.findById(1);
        expect(targetBici.id).toBe(1)
        expect(targetBici.color).toBe(a.color)
    });
});


describe('Bicicleta.delete', () => {
    it('Borrar bicicleta con id 1', () => {
        expect(Bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(1, 'rojo', 'urbana', [6.2260568, -75.5999279]);
        var b = new Bicicleta(2, 'azul', 'urbana', [6.23083494, -75.55701256]);
        Bicicleta.add(a);
        Bicicleta.add(b);
        expect(Bicicleta.allBicis.length).toBe(2);
        Bicicleta.removeById(1);
        expect(Bicicleta.allBicis[0]).toBe(b);
    });
});